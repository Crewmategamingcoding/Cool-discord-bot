import aiohttp
import discord
import json
from discord.ext import commands

def get_prefix(client, message):
    with open("prefixes.json", "r") as f:
        prefixes = json.load(f)

    return prefixes[str(message.guild.id)]

client = commands.Bot(command_prefix= get_prefix)
client.remove_command("help")


loadtoken = open("token.txt", "r")
token = loadtoken.read()
#change prefix command
@client.command()
async def changeprefix(ctx, prefix):
    with open("prefixes.json", "r") as f:
        prefixes = json.load(f)

    prefixes[str(ctx.guild.id)]= prefix
    with open("prefixes.json", "w") as f:
        json.dump(prefixes, f, indent=4)
# help command (UPDATE WHEN THERE'S A NEW COMMAND)
@client.command()
async def help(ctx):
    embed = discord.Embed(title="help command", description="Here's a list of commands to use me properly", colour=discord.Color.purple(), url="https://discord.gg/UqbgF8s")
    embed.set_author(name=client.user.name, icon_url=client.user.avatar_url)
    embed.set_footer(text=ctx.author.name, icon_url=ctx.author.avatar_url)
    embed.add_field(name="neko", value="this command gets a random neko image", inline=True)
    embed.add_field(name="kitsune", value="This gets a image of a random sfw kitsune", inline=False)
    embed.add_field(name="fox", value="gets a random image of a fox", inline=False)
    embed.add_field(name="panda", value="gets a random image of a panda", inline=False)
    embed.add_field(name="invite", value="this gets the invite link so you can add it to your own server!", inline=True)
    embed.add_field(name="moderation commands", value="These commands require you to have certain permissions to use", inline=False)
    embed.add_field(name="kick", value="kicks members when you mention them (requires kick permission)", inline=False)
    embed.add_field(name="ban", value="bans members when you mention them with the command (requires ban permissions)", inline=True)
    embed.add_field(name="changeprefix", value="changes the serverprefix for your server (requires admin permissions)", inline=False)
    await ctx.send(embed=embed)
# help command NSFW commands
@client.command(aliases=["help nsfw", "nsfw commands"])
async def help2(ctx):
    embed = discord.Embed(title="help command", description="Here's a list of commands to use me properly", colour=discord.Color.purple(), url="https://discord.gg/UqbgF8s")
    embed.set_author(name=client.user.name, icon_url=client.user.avatar_url)
    embed.set_footer(text=ctx.author.name, icon_url=ctx.author.avatar_url)
    embed.add_field(name="nekolewd", value="this gets a random NSFW neko image", inline=True)
    embed.add_field(name="hentai", value="this gets a random NSFW hentai image", inline=False)
    embed.add_field(name="aheago", value="random aheago image", inline=False)
    embed.add_field(name="nekolewd", value="this gets a random NSFW hentai image", inline=False)
    await ctx.send(embed=embed)
#ping command
@client.command()
async def ping(ctx):
    await ctx.send(f"pong! {round(client.latency * 1000)}ms")
# these commands get images from API services
@client.command()
async def redpanda(ctx):
    async with aiohttp.ClientSession() as cs:
        async with cs.get("https://some-random-api.ml/img/red_panda") as r:
            res = await r.json()  # returns dict
        await ctx.send(res["link"])

@client.command()
async def credits(ctx):
    await ctx.send(f"this bot was made by sandpaper Source for this bot is located at https://gitlab.com/-/ide/project/Crewmategamingcoding/Cool-discord-bot")


@client.command()
async def fox(ctx):
    async with aiohttp.ClientSession() as cs:
        async with cs.get("https://some-random-api.ml/img/fox") as r:
            res = await r.json()  # returns dict
        await ctx.send(res["link"])


@client.command()
async def neko(ctx):
    async with aiohttp.ClientSession() as cs:
        async with cs.get("https://neko-love.xyz/api/v1/neko") as r:
            res = await r.json()  # returns dict
        await ctx.send(res["url"])

@client.command()
async def foxgirl(ctx):
    async with aiohttp.ClientSession() as cs:
        async with cs.get("https://neko-love.xyz/api/v1/kitsune/") as r:
            res = await r.json()  # returns dict
        await ctx.send(res["url"])
@client.command()
@commands.is_nsfw()
async def nekolewd(ctx):
    async with aiohttp.ClientSession() as cs:
        async with cs.get("https://neko-love.xyz/api/v1/nekolewd") as r:
            res = await r.json()  # returns dict
        await ctx.send(res["url"])

@client.command()
@commands.is_nsfw()
async def hentai(ctx):
    async with aiohttp.ClientSession() as cs:
        async with cs.get("http://api.nekos.fun:8080/api/hentai") as r:
            res = await r.json()  # returns dict
        await ctx.send(res["url"])

@client.command()
@commands.is_nsfw()
async def aheago(ctx):
    async with aiohttp.ClientSession() as cs:
        async with cs.get("http://api.nekos.fun:8080/api/gasm") as r:
            res = await r.json()  # returns dict
        await ctx.send(res["url"])
#nekolewd error handler
@nekolewd.error
async def nekolewderror(ctx, error):
    if isinstance(error, commands.NSFWChannelRequired):
        await ctx.send("This channel is not marked with ``NSFW``")
@hentai.error
async def hentaierror(ctx, error):
    if isinstance(error, commands.NSFWChannelRequired):
        await ctx.send("This channel is not marked with ``NSFW``")
@aheago.error
async def aheagoerror(ctx, error):
    if isinstance(error, commands.NSFWChannelRequired):
        await ctx.send("This channel is not marked with ``NSFW``")

@client.command()
async def panda(ctx):
    async with aiohttp.ClientSession() as cs:
        async with cs.get("https://some-random-api.ml/img/panda") as r:
            res = await r.json()  # returns dict
        await ctx.send(res["link"])

# invite
@client.command(aliases=["invite"])
async def n(ctx, amount=10):
    await ctx.send("here is the invite!: https://discord.com/oauth2/authorize?client_id=769302837904932904&scope=bot&permissions=2080767231")

#moderation commands
@client.command(aliases=["purge","clear","removemessages"])
@commands.has_permissions(manage_messages=True)
async def a(ctx, amount=10):
    await ctx.channel.purge(limit=amount)



@client.command()
@commands.has_permissions(kick_members=True)
async def kick(ctx, member : discord.Member, *,reason=None):
    await member.kick(reason=reason)
@client.command()
@commands.has_permissions(ban_members=True)
async def ban(ctx, member : discord.Member, *,reason=None):
    await member.ban(reason=reason)
    await ctx.send(f"banned {member.mention}")


@client.event
async def on_guild_join(guild):
    with open("prefixes.json", "r") as f:
        prefixes = json.load(f)

    prefixes[str(guild.id)]= "c!"
    with open("prefixes.json", "w") as f:
        json.dump(prefixes, f, indent=4)


@client.event
async def on_guild_remove(guild):
    with open("prefixes.json", "r") as f:
        prefixes = json.load(f)

    prefixes.pop(str(guild.id))

    with open("prefixes.json", "w") as f:
        json.dump(prefixes, f, indent=4)

@client.event
async def on_ready():
    print("troled")


client.run(token)
